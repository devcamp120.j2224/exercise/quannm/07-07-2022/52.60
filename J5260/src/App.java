import java.util.ArrayList;
import java.util.Date;

import com.devcamp.j5260.Order2;
import com.devcamp.j5260.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        ArrayList<Order2> listOrder2 = new ArrayList<Order2>();

        // Khởi tạo các order
        Order2 order1 = new Order2();
        Order2 order2 = new Order2(0306, "BoiHB", 300000);
        Order2 order3 = new Order2(2005, "ThuNHM", 230000, new Date(), true);
        Order2 order4 = new Order2(1973, "ThanhHT", 320000, new Date(), true, new String[] {"Book", "Pen"}, new Person("Huynh Thi Thanh", 33, 52, new String[] {"Dog"}));
        
        // Thêm vào array
        listOrder2.add(order1);
        listOrder2.add(order2);
        listOrder2.add(order3);
        listOrder2.add(order4);

        // In ra
        for (Order2 order : listOrder2) {
            System.out.println(order.toString());
        }
    }
}
